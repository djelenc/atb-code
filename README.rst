=============
Alpha Testbed
=============

Alpha Testbed (ATB) is a tool for benchmarking trust and reputation models. 

This repository contains the source files of the project.

For additional information, please visit ATB's homepage at http://atb.fri.uni-lj.si.
